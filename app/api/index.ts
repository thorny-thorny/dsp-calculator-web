import { Item } from '@app/entities'
import { mockedItemImageForId } from '@app/mocks'
import { cast } from '@app/utils'

interface ApiItem {
  id: string
  label: string
}

export const getItems = async () => {
  const response = await fetch('http://localhost/api/item')
  const apiItems = await (response.json() as Promise<ApiItem[]>)

  return apiItems.map(apiItem => cast<Item>({
    id: apiItem.id,
    label: apiItem.label,
    image: mockedItemImageForId(apiItem.id),
  }))
}
