import { useContext } from 'react'
import { useParams } from 'react-router-dom'
import { nameForType } from '@app/entities'
import { ItemsContext } from '@app/context/items'
import { RecipeFormula, TagsList } from '@app/components'
import { RouteParams } from '@app/support'
import './styles.css'

export const ItemPage = () => {
  const { items, recipes } = useContext(ItemsContext)
  const { [RouteParams.ItemId]: itemId } = useParams()

  const item = items.find(item => item.id === itemId)!
  const itemRecipes = recipes.filter(recipe => recipe.outputs.some(bundle => bundle.item === item))
  const primaryRecipe = itemRecipes.find(recipe => !recipe.isCustom) ?? null

  return (
    <>
      <div className='row'>
        <div className='col-12 col-md-4 col-xl-2'>
          <div className='image-container mx-5 mx-md-0'>
            <img src={item.image} className='image' />
          </div>
        </div>
        <div className='col'>
          <h1 className='mt-3 mt-md-0'>{item.label}</h1>
          <p>{item.description}</p>
          <p><b>Type: </b>{nameForType(item.type)}</p>
          <p><b>Tags: </b><TagsList tags={item.tags} /></p>
          {primaryRecipe && (
            <p><b>Hand-make: </b>Replicator</p>
          )}
        </div>
      </div>
      {itemRecipes.length > 0 && (
        <h1 className='mt-3'>Recipes</h1>
      )}
      {itemRecipes.map(recipe => (
        <RecipeFormula recipe={recipe} />
      ))}
    </>
  )
}
