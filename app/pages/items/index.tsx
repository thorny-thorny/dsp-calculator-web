import { useContext } from 'react'
import { Link } from 'react-router-dom'
import { nameForType } from '@app/entities'
import { ItemsContext } from '@app/context/items'
import { TagsList } from '@app/components'
import './styles.css'

export const ItemsPage = () => {
  const { items } = useContext(ItemsContext)

  return (
    <div>
      <h3>All items</h3>
      <table className='table table-bordered table-striped'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Tags</th>
          </tr>
        </thead>
        <tbody>
          {items.map(item => (
            <tr>
              <td>
                <Link to={`/items/${encodeURIComponent(item.id)}`}><img src={item.image} className="item-icon me-1" />{item.label}</Link>
              </td>
              <td>{nameForType(item.type)}</td>
              <td>
                <TagsList tags={item.tags} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}
