import { AdvancedRecipe, BasicItem, Building, Component, Item, Material, NaturalResource, ProductionBuilding, Recipe, Tag, Type } from '@app/entities'
import ironOreImage from '@images/iron-ore.png'
import copperOreImage from '@images/copper-ore.png'
import ironIngotImage from '@images/iron-plate.png'
import copperIngotImage from '@images/copper-plate.png'
import smelterImage from '@images/smelter.png'
import gearImage from '@images/gear-wheel.png'
import magnetImage from '@images/magnet.png'
import magneticCoilImage from '@images/magnetism-wire.png'
import electricMotorImage from '@images/electric-motor.png'
import assembler1Image from '@images/assembler-1.png'
import { cast } from '@app/utils'

// TODO: get rid of all mocked data one day
export const mockedItemImageForId = (id: string) => {
  switch (id) {
    case 'steel':
      return 'steel-plate.png'
    case 'siliconIngot':
      return 'silicium-single-crystal.png'
    case 'ironIngot':
      return 'iron-plate.png'
    case 'hydrogen':
      return 'hydrogen.png'
    case 'graphene':
      return 'graphene.png'
    case 'acid':
      return 'sulphuric-acid.png'
    case 'deuterium':
      return 'deuterium-energy-fuel.png'
    case 'crudeOil':
      return 'oil.png'
    case 'microcrystallineComponent':
      return 'micro-component.png'
    case 'prism':
      return 'prism.png'
    case 'energeticGraphite':
      return 'graphite.png'
    case 'glass':
      return 'glass.png'
    case 'solarSail':
      return 'solar-collector.png'
    case 'siliconOre':
      return 'silicium-ore.png'
    case 'photonCombiner':
      return 'photo-shifter.png'
    case 'water':
      return 'water.png'
    case 'processor':
      return 'processor.png'
    case 'stone':
      return 'stone-ore.png'
    case 'refinedOil':
      return 'refined-oil.png'
    case 'circuitBoard':
      return 'circuit-board.png'
    case 'titaniumIngot':
      return 'titanium-plate.png'
    case 'coal':
      return 'coal-ore.png'
    case 'ironOre':
      return 'iron-ore.png'
    case 'copperIngot':
      return 'copper-plate.png'
    case 'copperOre':
      return 'copper-ore.png'
    case 'titaniumOre':
      return 'titanium-ore.png'
    default:
      return 'photon-capacitor-full.png'
  }
}

const ironOre = cast<NaturalResource>({
  id: 'ironOre',
  label: 'Iron ore',
  image: ironOreImage,
  description: 'Used to smelt iron ingots and magnets. Obtained by gathering iron vein.',
  type: Type.Item,
  tags: [Tag.NaturalResource],
  gatheredFrom: 'Iron ore',
})

const copperOre = cast<NaturalResource>({
  id: 'copperOre',
  label: 'Copper ore',
  image: copperOreImage,
  description: 'Smelted directly into copper ingots. Obtained by gathering copper vein.',
  type: Type.Item,
  tags: [Tag.NaturalResource],
  gatheredFrom: 'Ore vein'
})

const ironIngot = cast<Material>({
  id: 'ironIngot',
  label: 'Iron ingot',
  image: ironIngotImage,
  description: 'Basic raw material. Used to produce various iron components.',
  type: Type.Item,
  tags: [Tag.Material],
})

const copperIngot = cast<Material>({
  id: 'copperIngot',
  label: 'Copper ingot',
  image: copperIngotImage,
  description: 'Basic raw material. Used to produce various copper components.',
  type: Type.Item,
  tags: [Tag.Material],
})

const magnet = cast<Material>({
  id: 'magent',
  label: 'Magnet',
  image: magnetImage,
  description: 'Basic magnetic material. Obtained by smelting iron ore directly.',
  type: Type.Item,
  tags: [Tag.Material],
})

const magneticCoil = cast<Component>({
  id: 'magneticCoil',
  label: 'Magnetic coil',
  image: magneticCoilImage,
  description: 'The most basic electromagnetic component. It is very functional.',
  type: Type.Item,
  tags: [Tag.Component],
})

const electricMotor = cast<Component>({
  id: 'electricMotor',
  label: 'Electric motor',
  image: electricMotorImage,
  description: 'The fundamental power system component. It can accelerate mechanical operation.',
  type: Type.Item,
  tags: [Tag.Component],
})

const gear = cast<Component>({
  id: 'gear',
  label: 'Gear',
  image: gearImage,
  description: 'Standard component is used for transmission and other functions and is also widely used in various mechanical structures.',
  type: Type.Item,
  tags: [Tag.Component],
})

const smelter = cast<ProductionBuilding>({
  id: 'smelter',
  label: 'Smelter',
  image: smelterImage,
  description: 'High temperature arc smelting can smelt ores and metals. and also be used for purification and crystal smelting.',
  type: Type.Building,
  tags: [Tag.SmeltingFacility],
  productionSpeed: 1,
  workConsumption: 360000,
  idleConsumption: 12000,
})

const assembler1 = cast<ProductionBuilding>({
  id: 'assembler1',
  label: 'Assembling machine Mk.I',
  image: assembler1Image,
  description: 'The basic assembler. Can process iron and copper ingots into more advanced products.',
  type: Type.Building,
  tags: [Tag.Assembler],
  productionSpeed: 0.75,
  workConsumption: 270000,
  idleConsumption: 12000,
})

export const mockedItems: (Item | Building)[] = [
  ironOre,
  copperOre,
  ironIngot,
  copperIngot,
  magnet,
  magneticCoil,
  electricMotor,
  gear,
  smelter,
  assembler1,
]

const ironIngotRecipe = cast<Recipe>({
  inputs: [{ item: ironOre, amount: 1 }],
  outputs: [{ item: ironIngot, amount: 1 }],
  facility: smelter,
  handMake: true,
  rate: 1,
  isCustom: false,
})

const copperIngotRecipe = cast<Recipe>({
  inputs: [{ item: copperOre, amount: 1 }],
  outputs: [{ item: copperIngot, amount: 1 }],
  facility: smelter,
  handMake: true,
  rate: 1,
  isCustom: false,
})

const magnetRecipe = cast<Recipe>({
  inputs: [{ item: ironOre, amount: 1 }],
  outputs: [{ item: magnet, amount: 1 }],
  facility: smelter,
  handMake: true,
  rate: 1.5,
  isCustom: false,
})

const magneticCoilRecipe = cast<Recipe>({
  inputs: [{ item: magnet, amount: 2 }, { item: copperIngot, amount: 1 }],
  outputs: [{ item: magneticCoil, amount: 2 }],
  facility: assembler1,
  handMake: true,
  rate: 1,
  isCustom: false,
})

const gearRecipe = cast<Recipe>({
  inputs: [{ item: ironIngot, amount: 1 }],
  outputs: [{ item: gear, amount: 1 }],
  facility: assembler1,
  handMake: true,
  rate: 1,
  isCustom: false,
})

const electricMotorRecipe = cast<Recipe>({
  inputs: [{ item: ironIngot, amount: 2 }, { item: gear, amount: 1 }, { item: magneticCoil, amount: 1 }],
  outputs: [{ item: electricMotor, amount: 1 }],
  facility: assembler1,
  handMake: true,
  rate: 2,
  isCustom: false,
})

export const mockedRecipes: (Recipe | AdvancedRecipe)[] = [
  ironIngotRecipe,
  copperIngotRecipe,
  magnetRecipe,
  magneticCoilRecipe,
  gearRecipe,
  electricMotorRecipe,
]
