import React from 'react'
import { mockedItems, mockedRecipes } from '@app/mocks'

export const ItemsContext = React.createContext({
  items: mockedItems,
  recipes: mockedRecipes,
})
