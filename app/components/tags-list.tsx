import { nameForTag, Tag } from '@app/entities'

interface Props {
  tags: Tag[]
}

export const TagsList = (props: Props) => (
  <>
    {props.tags.map((tag, index) => (
      <span className='badge rounded-pill bg-secondary' key={index}>{nameForTag(tag)}</span>
    ))}
  </>
)
