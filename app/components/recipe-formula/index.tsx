import { Link } from 'react-router-dom'
import { AdvancedRecipe, Recipe } from '@app/entities'
import './styles.css'

interface Props {
  recipe: Recipe | AdvancedRecipe
}

export const RecipeFormula = ({ recipe }: Props) => (
  <p>
    {recipe.outputs.map((output, index) => (
      <span>
        {index > 0 && '+'}
        {output.amount}
        <Link to={`/items/${encodeURIComponent(output.item.id)}`}><img src={output.item.image} className='item-icon' /></Link>
      </span>
    ))}
    <span> ← ({recipe.rate}s) </span>
    {recipe.inputs.map((input, index) => (
      <span>
        {index > 0 && '+'}
        {input.amount}
        <Link to={`/items/${encodeURIComponent(input.item.id)}`}><img src={input.item.image} className='item-icon' /></Link>
      </span>
    ))}
  </p>
)
