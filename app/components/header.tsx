import { Link, NavLink } from 'react-router-dom'
import { RouteNames } from '@app/support'
import { className } from '@app/utils'

interface HeaderNavItemProps {
  route: string
  children: string
}

const HeaderNavItem = ({ route, children }: HeaderNavItemProps) => (
  <li className='nav-item' key={route}>
    <NavLink to={route} className={({ isActive }) => className('nav-link', isActive && 'active')}>{children}</NavLink>
  </li>
)
 
export const Header = () => {
  const links = ['Resources', 'Items', 'Buildings']

  return (
    <div className='container'>
      <header className='d-flex flex-wrap justify-content-center py-3 mb-3 border-bottom'>
        <Link to='/' className='mb-3 mb-md-0 me-md-auto'>Dyson Sphere Program calculator</Link>
        <ul className='nav nav-pills'>
          <HeaderNavItem route={RouteNames.Items}>Items</HeaderNavItem>
          {/* <HeaderNavItem route={RouteNames.Resources}>Resources</HeaderNavItem> */}
        </ul>
      </header>
    </div>
  )
}
