import { HashRouter, Route, Routes } from 'react-router-dom'
import { MainPage, ItemsPage, ItemPage } from '@app/pages'
import { Header } from '@app/components'
import { RouteNames, RouteParams } from '@app/support'

export const App = () => {
  return (
    <HashRouter>
      <Header />
      <div className='container'>
        <Routes>
          <Route path={RouteNames.Index} element={<MainPage />} />
          <Route path={RouteNames.Items} element={<ItemsPage />} />
          <Route path={`${RouteNames.Items}/:${RouteParams.ItemId}`} element={<ItemPage />} />
        </Routes>
      </div>
    </HashRouter>
  )
}
