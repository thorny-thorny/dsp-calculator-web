export enum Type {
  Item,
  Building,
}

export enum Tag {
  NaturalResource,
  Material,
  Component,
  EndProduct,
  Logistics,
  ScienceMatrix,
  SmeltingFacility,
  Assembler,
}

export type BasicItem<Type> = {
  id: string
  label: string
  image: string
  description: string
  type: Type
  tags: Tag[]
}

export type Item = BasicItem<Type.Item>

export type NaturalResource = Item & {
  gatheredFrom: string
}

export type Material = Item

export type Component = Item

export type Building = BasicItem<Type.Building>

export type ProductionBuilding = Building & {
  productionSpeed: number
  workConsumption: number
  idleConsumption: number
}

export type ItemBundle = {
  item: Item | Building
  amount: number
}

export type BasicRecipe<CustomType> = {
  inputs: ItemBundle[]
  outputs: ItemBundle[]
  facility: ProductionBuilding
  handMake: boolean
  rate: number
  isCustom: CustomType
}

export type Recipe = BasicRecipe<false>

export type AdvancedRecipe = BasicRecipe<true> & {
  label: string
  description: string
  image: string
}

export const nameForTag = (tag: Tag) => {
  switch (tag) {
    case Tag.NaturalResource:
      return 'Natural resource'
    case Tag.Material:
      return 'Material'
    case Tag.Component:
      return 'Component'
    case Tag.EndProduct:
      return 'End product'
    case Tag.Logistics:
      return 'Logistics'
    case Tag.ScienceMatrix:
      return 'Science matrix'
    case Tag.SmeltingFacility:
      return 'Smelting facility'
    case Tag.Assembler:
      return 'Assembler'
  }
}

export const nameForType = (type: Type) => {
  switch (type) {
    case Type.Item:
      return 'Item'
    case Type.Building:
      return 'Building'
  }
}
