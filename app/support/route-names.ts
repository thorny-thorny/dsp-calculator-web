export enum RouteNames {
  Index = '/',
  Resources = '/resources',
  Items = '/items',
}

export enum RouteParams {
  ItemId = 'itemId',
}
