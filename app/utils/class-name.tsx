export const className = (...names: (string | boolean | null | undefined)[]) => {
  return names.filter(name => name).join(' ')
}
