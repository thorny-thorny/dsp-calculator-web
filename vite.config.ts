const path = require('path')
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      '@app': path.resolve(__dirname, 'app'),
      '@images': path.resolve(__dirname, 'static', 'images'),
      '@styles': path.resolve(__dirname, 'static', 'styles'),
    }
  },
})
